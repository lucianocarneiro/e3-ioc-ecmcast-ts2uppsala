############################################################
############# TS2 Incoming Reception Area EtherCAT HW configuration

#Configure EL1100 EtherCAT Coupler
${SCRIPTEXEC} ${ecmccfg_DIR}addSlave.cmd, "SLAVE_ID=0, HW_DESC=EK1100"

#Configure EL70471-0052 stepper drive terminal
${SCRIPTEXEC} ${ecmccfg_DIR}configureSlave.cmd, "SLAVE_ID=3, HW_DESC=EL7041-0052, CONFIG=-Motor-Phytron-VSS-32.200.1.2"
#
# Configure reduced current 0 mA
ecmcConfigOrDie "Cfg.EcAddSdo(${ECMC_EC_SLAVE_NUM},0x8010,0x2,0,2)"

#Apply hardware configuration
ecmcConfigOrDie "Cfg.EcApplyConfig(1)"

#Configure EL70471-0052 stepper drive terminal
${SCRIPTEXEC} ${ecmccfg_DIR}configureSlave.cmd, "SLAVE_ID=4, HW_DESC=EL7041-0052, CONFIG=-Motor-Phytron-VSS-32.200.1.2"
#
# Configure reduced current 0 mA
ecmcConfigOrDie "Cfg.EcAddSdo(${ECMC_EC_SLAVE_NUM},0x8010,0x2,0,2)"

#Apply hardware configuration
ecmcConfigOrDie "Cfg.EcApplyConfig(1)"

